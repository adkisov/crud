<?php

require_once 'config/connect.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <title>Products</title>
</head>
<body>
<div class="table">
<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth" >
  <thead>
    <tr>
      <th><abbr title="ID">ID</abbr></th>
      <th>Title</th>
      <th><abbr title="Description">Description</abbr></th>
      <th><abbr title="Price">Price</abbr></th>
    </tr>
      <?php 
        $products = mysqli_query($connect, "SELECT * FROM products");
        $products =mysqli_fetch_all($products);
        foreach($products as $product):
            ?>
                <tr>
                    <td><?=$product[0]?></td>
                    <td><?=$product[1]?></td>
                    <td><?=$product[3]?></td>
                    <td><?=$product[2]?></td>
                    <td><a href = "update.php?id=<?=$product[0]?>";"> <ion-icon name="create"></ion-icon></td> </a>
                    <td><a href="vendor/delete.php?id=<?=$product[0]?>";"> <ion-icon name="trash"></ion-icon> </a></td>
                </tr>
        <?php    
        endforeach
    ?>

  </thead>
</table>
            <label class="label">add new product</label>

            <form action="vendor/create.php" method="post">
               <p>tittle</p>
               <input class="input is-primary" type="text" name="tittle" id="">
               <p>description</p>
               <textarea class="input is-info" name="description" id=""> </textarea>
               <p>price</p>
               <input class="input is-primary" type="number" name="price" id=""> <br>
               <button class="button is-primary" type="submit"> add new product </button>
               

            </form>
</div>
</body>
</html>